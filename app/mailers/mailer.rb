class Mailer < ActionMailer::Base
  layout 'mail'
  default from: "neworder@pretty-people.com.ua"

  def admin_order(order)
    @order = order

    mail(to: 'neonilla.xlash@gmail.com', subject: "Поступил новый заказ") do |format|
      format.html { render 'admin_order' }
    end
  end

  def admin_payment(order)
    @order = order

    mail(to: 'neonilla.xlash@gmail.com', subject: "Поступила оплата заказа") do |format|
      format.html { render 'admin_payment' }
    end
  end

  def admin_phonecall(phonecall)
    @phonecall = phonecall

    mail(to: 'neonilla.xlash@gmail.com', subject: "Обратный звонок") do |format|
      format.html { render 'admin_phonecall' }
    end
  end

  def customer_order(order)
    @order = order

    mail(to: @order.user.email, subject: "PrettyPeople заказ принят") do |format|
      format.html { render 'customer_order' }
    end
  end

  def customer_payment(order)
    @order = order

    mail(to: @order.user.email, subject: "PrettyPeople оплата получена") do |format|
      format.html { render 'customer_payment' }
    end
  end

end
