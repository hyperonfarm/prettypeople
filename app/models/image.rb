class Image < ActiveRecord::Base

  belongs_to :product

  has_attached_file :file, styles: {  thumb: "62x83#", zoom: "900x1200#" }, default_style: :original

  validates_attachment :file,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

end
