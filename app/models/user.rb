class User < ActiveRecord::Base
  devise :database_authenticatable, :confirmable,
         :rememberable,:recoverable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook, :vkontakte]


  attr_accessor :current_password

  has_many :social_accounts, dependent: :destroy

  has_attached_file :avatar, :styles => { :medium => "140x140#", :thumb => "38x38#", :chat => "65x65#" }, :default_url =>  "avatar/avatar-default.png"
  validates_attachment :avatar,
                              content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                              size: { in: 0..5.megabytes }

  def self.from_omniauth(auth)
    social_account = SocialAccount.where(provider: auth.provider, uid: auth.uid).first_or_create
    if social_account.user
      user = social_account.user
    else social_account.user
      user = User.new()
      user.skip_confirmation!
      user.email = auth.info.email
      user.name = auth.info.name
      user.save
      social_account.update_attribute(:user_id, user.id)
      user.avatar = auth.info.image
      user.save
    end
    user
    end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
      if data = session["devise.vkontakte_data"] && session["devise.vkontakte_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def email_required?
    super && SocialAccount.where(:user => self).blank?
  end

  def password_required?
    super && SocialAccount.where(:user => self).blank?
  end

  def password_required?
    # Password is required if it is being set, but not for new records
    if !persisted?
      false
    else
      !password.nil? || !password_confirmation.nil?
    end
  end
end
