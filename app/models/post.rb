class Post < ActiveRecord::Base
  before_save :check_seo

  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, :presence => true

  default_scope -> { order('created_at DESC') }

  scope :published, -> { where('published = ?', true) }
  scope :promo, -> { where('published = ? AND promo = ?', true, true) }

  has_attached_file :image, styles: {  thumb: "90x80#", index: "900x800", original: "1680x1050#" }, default_style: :original

  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  def check_seo
    if self.seo_title.blank?
      self.seo_title = self.title
    end

    if self.seo_description.blank?
      self.seo_description = self.description unless self.description.blank?
    end
  end

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

end
