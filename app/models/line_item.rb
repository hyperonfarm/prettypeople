class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product

  validates :product, :price, :quantity, presence: true
  validates :price, numericality: true
  validates :quantity, numericality: { integer_only: true, greater_than_or_equal_to: 0 }
  validates :product, uniqueness: { scope: :order }

  before_validation :set_price

  def total
    price * quantity
  end

  private

  def set_price
    self.price = product.price
  end
end
