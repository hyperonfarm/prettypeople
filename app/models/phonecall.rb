class Phonecall < ActiveRecord::Base
  default_scope -> { order('created_at DESC') }
  validate :phone, presence: true


  def send_notifications
    Mailer.admin_phonecall(self).deliver
  end
end
