class Category < ActiveRecord::Base
  before_save :check_seo

  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, :presence => true

  has_many :products, dependent: :destroy

  has_attached_file :image, styles: { thumb: '100x', admin_show: '350x', index: '753x304#', original: '900x1200#'}, default_style: :original
  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  scope :published, -> { where('published = ?', true) }
  default_scope -> { order('position DESC') }

  def check_seo
    if self.seo_title.blank?
      self.seo_title = self.title
    end

    if self.seo_description.blank?
      self.seo_description = self.description unless self.description.blank?
    end
  end

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

end
