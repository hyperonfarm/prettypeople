class Slide < ActiveRecord::Base
  default_scope -> { order('priority DESC') }

  has_attached_file :image, styles: { original: '1920x1080#', admin_show: '720x350#', admin_index: '144x70#' }, default_style: :original

  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..10.megabytes }

  def self.styles
    ["1", "2"]
  end
end
