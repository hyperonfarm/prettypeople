class Order < ActiveRecord::Base
  belongs_to :user
  after_create :set_condition

  scope :completed, -> { where(condition: 'Новый') }
  scope :finished, -> { where(condition: "Обработан") }
  scope :not_completed, -> { where('ordered_at IS NULL') }
  scope :not_ordered, -> { where(:condition => "Отменен") }
  scope :total_completed, -> { where('ordered_at IS NOT NULL') }

  has_many :line_items, dependent: :destroy
  accepts_nested_attributes_for :line_items, allow_destroy: true
  validates :user, presence: true

  def total
    line_items.to_a.sum(&:total)
  end

  def set_condition
    self.condition = "Новый"
  end

  def self.conditions
    ["Новый", "Обработан", "Отменен"]
  end

  def self.deliveries
    ["Самовывоз", "По Киеву", "Новая почта", "Интайм"]
  end

  def send_notifications
    Mailer.admin_order(self).deliver
    Mailer.customer_order(self).deliver
  end
end
