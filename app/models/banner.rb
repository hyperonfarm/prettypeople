class Banner < ActiveRecord::Base
  has_attached_file :image, styles: {  thumb: "100x", show: "300x", right: "282x377#", top: '848x254#', bottom: '848x340#' }, default_style: :original

  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  default_scope -> { order('created_at DESC') }
end
