class Product < ActiveRecord::Base
  before_save :check_seo

  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, :presence => true

  belongs_to :category

  has_many :images, dependent: :destroy
  accepts_nested_attributes_for :images, :allow_destroy => true

  has_many :details, dependent: :destroy
  accepts_nested_attributes_for :details, :allow_destroy => true

  has_attached_file :image, styles: { thumb: '100x', index: '753x304#', show: '750x1000#', home: '750x940#', cart_thumb: '53x70#' }, default_style: :original
  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  scope :promo, -> { where('promo_on = ? AND promo_date >= ?', true, Time.now) }
  scope :published, -> { where('published = ?', true) }

  def self.search(query)
    where("title || description ILike ?", "%#{query}%")
  end

  def self.badges
    ['Новинка', 'Хит продаж', 'Акция', 'Заканчивается']
  end

  def check_seo
    if self.seo_title.blank?
      self.seo_title = self.title
    end

    if self.seo_description.blank?
      self.seo_description = self.description unless self.description.blank?
    end
  end


  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end
end
