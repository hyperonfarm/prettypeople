class SmallSlide < ActiveRecord::Base
  has_attached_file :image, styles: {  thumb: "56x37#", original: "565x377#" }, default_style: :original

  validates_attachment :image,
    content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
    size: { in: 0..5.megabytes }

  default_scope -> { order('created_at DESC') }
end
