ActiveAdmin.register Category do
  menu priority: 3

  config.filters = false

  permit_params :title, :position, :published, :description, :image, :seo_title, :seo_keywords, :seo_description

  config.filters = false
  config.sort_order = 'position'

  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  index download_links: false do
    column :title
    column :position
    column :published do |prod|
      case prod.published
        when true
          status_tag('Да', :ok)
        when false
          status_tag('Нет', :error)
      end
    end
    actions
  end

  show do |f|
    attributes_table do
      row :published do |prod|
        case prod.published
          when true
            status_tag('Да', :ok)
          when false
            status_tag('Нет', :error)
        end
      end
      row :position
      row :title
      row :description
      row :image do |f|
        image_tag(f.image.url(:admin_show))
      end
    end

    unless category.products.blank?
      panel 'Товары' do
        table_for category.products do
          column "Наименование" do |d|
            d.title
          end
          column "Артикул" do |d|
            d.article
          end
          column "Цена" do |d|
            d.price
          end
        end
      end
    end
  end

  form do |f|
    f.inputs name: "Категория" do
      f.input :published
      f.input :position
      f.input :image, image_preview: true
      f.input :title
      f.input :description
    end

    f.inputs "SEO" do
      f.input :seo_title
      f.input :seo_keywords, input_html: { :rows => 4 }
      f.input :seo_description, input_html: { :rows => 4 }
    end

    f.actions
  end

end
