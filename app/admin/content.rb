ActiveAdmin.register Content do

  menu priority: 10

  permit_params :text

  actions :all, except: [:new, :destroy]
  config.filters = false
  config.sort_order = 'created_at_asc'

  index download_links: false do
    column :id
    column :title
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :text do |content|
        content.text.try(:html_safe)
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title, :input_html => { :disabled => true }
      f.input :text, as: :ckeditor
    end

    f.actions
  end

end
