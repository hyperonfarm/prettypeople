ActiveAdmin.register Post do
  menu priority: 6
  permit_params :title, :image, :description, :text, :promo, :published, :seo_title, :seo_keywords, :seo_description

  config.per_page = 50
  config.filters = false

  config.sort_order = 'created_at_desc'

  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  index :download_links => false do
    column :image do |image|
      image_tag image.image.url(:thumb)
    end
    column :title
    column :description
    column :published do |post|
      case post.published
        when true
          status_tag('Да', :ok)
        when false
          status_tag('Нет', :default)
      end
    end
    column :promo do |post|
      case post.promo
        when true
          status_tag('Да', :ok)
        when false
          status_tag('Нет', :default)
      end
    end
    column :created_at

    actions
  end

  show do |f|
    attributes_table do
      row :image do
        image_tag(f.image.url(:index))
      end
      row :created_at
      row :title
      row :description
      row :published do |post|
        case post.published
          when true
            status_tag('Да', :ok)
          when false
            status_tag('Нет', :default)
        end
      end
      row :promo do |post|
        case post.promo
          when true
            status_tag('Да', :ok)
          when false
            status_tag('Нет', :default)
        end
      end
      row :text do |post|
        post.text.try(:html_safe)
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :published
      f.input :promo
      f.input :image, required: false, image_preview: true
      f.input :title
      f.input :description, input_html: { :rows => 4 }
    end
    f.inputs do
      f.input :text, as: :ckeditor
    end
    f.inputs "SEO" do
      f.input :seo_title
      f.input :seo_keywords, input_html: { :rows => 4 }
      f.input :seo_description, input_html: { :rows => 4 }
    end
    f.actions
  end
end
