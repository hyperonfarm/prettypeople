ActiveAdmin.register Question do
  menu priority: 7
  permit_params :title, :answer, :priority

  config.sort_order = 'priority_desc'

  config.per_page = 40
  config.filters = false

  index :download_links => false do
    column :priority
    column :title
    column :answer do |t|
      unless t.send(:answer).blank?
        t.send(:answer).html_safe
      end
    end

    actions
  end

  show do
    attributes_table do
      row :priority
      row :title
      row :answer do |t|
        unless t.send(:answer).blank?
          t.send(:answer).html_safe
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :priority
      f.input :title
      f.input :answer, :input_html => { :rows => 5 }
    end
    f.actions
  end
end
