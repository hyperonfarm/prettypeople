ActiveAdmin.register Banner do
  menu parent: 'home', priority: 2, label: proc { I18n.t('admin.banner') }

  permit_params :image, :title, :url

  actions :all, except: [:new, :destroy]
  config.filters = false
  config.sort_order = 'created_at_asc'

  index :download_links => false do
    column :image do |image|
      image_tag image.image.url(:thumb)
    end
    column :title

    actions
  end

  show do |f|
    attributes_table do
      row :image do
        image_tag(f.image.url(:show))
      end
      row :title
      row :url
    end
  end

  form do |f|
    f.inputs do
      f.input :image, required: false, image_preview: true
      f.input :title
      f.input :url
    end
    f.actions
  end
end
