ActiveAdmin.register Order do
  menu priority: 2

  permit_params :admin_comment, :condition, :delivery, :city, :department, :status,
                line_items_attributes: [:id, :product_id, :price, :quantity, :_destroy]

  actions :all, except: [:new]

  filter :id, label: "Номер заказа"
  filter :ordered_at
  filter :user
  filter :city

  scope :completed, default: true
  scope :finished
  scope :not_ordered
  scope :all

  index do
    column :id
    column :user, sortable: :user_id
    column :ordered_at
    column :items do |order|
      unless order.line_items.blank?
        order.line_items.count
      end
    end
    column :total do |order|
      unless order.total.blank?
        "#{order.total} грн."
      end
    end
    column :condition do |order|
      case order.condition
        when "Новый"
          status_tag('Новый', :warning)
        when "Отменен"
          status_tag('Отменен', :default)
        when "Обработан"
          status_tag('Обработан', :ok)
      end
    end

    column :status do |order|
      case order.status
        when "wait_accept"
          status_tag('На проверке', :warning)
        when "processing"
          status_tag('Обработка', :warning)
        when "wait_secure"
          status_tag('Проверка', :warning)
        when "sandbox"
          status_tag('Тестовый', :default)
        when "failure"
          status_tag('Ошибка', :error)
        when "success"
          status_tag('Обработан', :ok)
      end
    end

    actions
  end

  show do |order|
    attributes_table do
      row :condition do |order|
        case order.condition
          when "Новый"
            status_tag('Новый', :warning)
          when "Отменен"
            status_tag('Отменен', :default)
          when "Обработан"
            status_tag('Обработан', :ok)
        end
      end
      row :status do |order|
        case order.status
          when "wait_accept"
            status_tag('На проверке', :warning)
          when "processing"
            status_tag('Обработка', :warning)
          when "wait_secure"
            status_tag('Проверка', :warning)
          when "sandbox"
            status_tag('Тестовый', :default)
          when "failure"
            status_tag('Ошибка', :error)
          when "success"
            status_tag('Обработан', :ok)
        end
      end
      row :id
      row :ordered_at
      row :user
      row :total do
        unless order.total.blank?
          "#{order.total} грн."
        end
      end
      row :delivery
      row :city
      row :department
    end
    table_for order.line_items, sortable: true, class: 'index_table' do
      column "Название" do |o|
        o.product.title
      end
      column "Артикул" do |o|
        o.product.article
      end
      column "Категория" do |line_item|
        if line_item.product.category
          line_item.product.category.title
        else
          "Без Категории"
        end
      end
      column "Цена", sortable: :price do |line_item|
        "#{line_item.price} грн."
      end
      column "К-во" do |line_item|
        "#{line_item.quantity}"
      end
      column "Всего" do |line_item|
        "#{line_item.total} грн."
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :condition, as: :select, collection: Order.conditions, include_blank: false
      f.input :admin_comment, input_html: { rows: 6 }
    end

    f.inputs "Доставка" do
      f.input :delivery, as: :select, collection: Order.deliveries
      f.input :city
      f.input :department
    end

    f.inputs "Корзина" do
      f.has_many :line_items, allow_destroy: true, new_record: true do |li|
        li.input :product
        li.input :quantity
        li.form_buffers.last << javascript_tag("$('select').last().chosen()")
      end
    end

    f.actions

  end
end
