ActiveAdmin.register Page do
  menu parent: 'settings', priority: 3, label: proc { I18n.t('admin.seo') }

  permit_params :seo_title, :seo_keywords, :seo_description

  actions :all, except: [:new, :destroy]
  config.filters = false
  config.sort_order = 'created_at_asc'

  index download_links: false do
    column :id
    column :title
    actions
  end

  form do |f|
    f.inputs do
      f.input :seo_title
      f.input :seo_keywords, :input_html => { :rows => 4 }
      f.input :seo_description, :input_html => { :rows => 4 }
    end

    f.actions
  end
end
