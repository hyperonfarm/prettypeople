ActiveAdmin.register Phonecall do
  menu priority: 9

  permit_params :phone

  config.per_page = 50
  filter :created_at

  actions :all, except: [:new]

  config.sort_order = 'created_at_desc'

  index :download_links => false do
    column :id
    column :phone

    actions
  end

  show do |f|
    attributes_table do
      row :id
      row :phone
    end
  end
end
