ActiveAdmin.register Product do
  menu priority: 4

  permit_params :title, :article, :price, :old_price, :description, :published, :badge, :discount_badge,
                :image, :category_id, :note,
                :promo_on, :promo_date,
                :full_description, :seo_title, :seo_keywords, :seo_description,
                :seo_title, :seo_description, :seo_keywords,
                :images_attributes => [:file, :_destroy, :id, :product_id],
                :details_attributes => [:value, :product_id, :_destroy, :id, :title]


  filter :category, :as => :select, collection: -> { Category.all.to_a.map do |cat|
          [cat.title, cat.id]
        end }

  controller do
    def find_resource
      scoped_collection.where(slug: params[:id]).first!
    end
  end

  index download_links: false do
    column :title
    column :article
    column :category, :sortable => :category_id
    column :price
    column :published do |prod|
      case prod.published
        when true
          status_tag('Да', :ok)
        when false
          status_tag('Нет', :error)
      end
    end
    column :promo_on do |prod|
      case prod.promo_on
        when true
          status_tag('Да', :ok)
      end
    end
    column :badge

    actions
  end

  show do |product|
    attributes_table do
      row :image do |f|
        image_tag(f.image.url)
      end
      row :published do |prod|
        case prod.published
          when true
            status_tag('Да', :ok)
          when false
            status_tag('Нет', :error)
        end
      end
      row :promo_on do |prod|
        case prod.promo_on
          when true
            status_tag('Да', :ok)
          when false
            status_tag('Нет', :default)
        end
      end
      row :promo_date
      row :title
      row :category, as: :select
      row :price
      row :old_price
      row :badge
      row :discount_badge
      row :description
      row :full_description do |post|
        post.full_description.try(:html_safe)
      end
      row :note do |post|
        post.note.try(:html_safe)
      end
    end

    panel 'Детальнее' do
      table_for product.details, class: 'index_table' do
        column "Наименование" do |d|
          d.title
        end
        column "Значение" do |d|
          d.value
        end
      end
    end

    attributes_table do
      row :images do
        product.images.each do |img|
          attributes_table_for img do
            row_i :file
          end
        end
      end
    end
  end

  form do |f|
    f.inputs :name => "Публикация" do
      f.input :published
      f.input :badge, as: :select, collection: Product.badges, include_blank: true
      f.input :discount_badge
    end

    f.inputs :name => "Разместить в промо-блоке" do
      f.input :promo_on
      f.input :promo_date
    end

    f.inputs do
      f.input :image, label: 'Изображение', image_preview: true
    end

    f.inputs do
      f.input :category
      f.input :title
      f.input :article
      f.input :description, input_html: { :rows => 4 }
      f.input :full_description, as: :ckeditor
      f.input :note, as: :ckeditor
    end

    f.inputs do
      f.input :old_price
      f.input :price
    end

    f.inputs "SEO" do
      f.input :seo_title
      f.input :seo_keywords, input_html: { :rows => 4 }
      f.input :seo_description, input_html: { :rows => 4 }
    end

    f.inputs 'Дополнительные аттрибуты' do
      f.has_many :details, :allow_destroy => true, :heading => '', :new_record => true do |d|
        d.input :title
        d.input :value
      end
    end


    f.inputs 'Изображения' do
      f.semantic_errors :base
      f.has_many :images, heading: '', allow_destroy: true, new_record: true do |cf|
        cf.input :file, label: 'Изображение', image_preview: true
      end
    end
    f.actions
  end

end
