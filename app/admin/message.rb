ActiveAdmin.register Message do
  menu priority: 8

  permit_params :name, :email, :text, :published

  config.per_page = 50
  filter :created_at

  actions :all, except: [:new]

  config.sort_order = 'created_at_desc'

  index :download_links => false do
    column :created_at
    column :name
    column :email
    column :text do |t|
      unless t.send(:text).blank?
        t.send(:text).truncate(70)
      end
    end
    column :published do |mess|
      case mess.published
        when true
          status_tag('Да', :ok)
        when false
          status_tag('Нет', :default)
      end
    end

    actions
  end

  show do |f|
    attributes_table do
      row :published do |mess|
        case mess.published
          when true
            status_tag('Да', :ok)
          when false
            status_tag('Нет', :default)
        end
      end
      row :name
      row :email
      row :text
    end
  end
end
