ActiveAdmin.register Slide do
  menu parent: 'home', priority: 1, label: proc { I18n.t('admin.slider') }

  permit_params :image, :priority, :title, :subtitle, :style, :url

  config.per_page = 10
  config.filters = false

  index :download_links => false do
    column :image do |image|
      image_tag image.image.url(:admin_index)
    end
    column :title
    column :subtitle
    column :priority

    actions
  end

  show do |f|
    attributes_table do
      row :image do
        image_tag(f.image.url(:admin_show))
      end
      row :title
      row :subtitle
      row :priority
    end
  end

  form do |f|
    f.inputs do
      f.input :image, required: false, image_preview: true
      f.input :title
      f.input :subtitle
      f.input :priority
      f.input :url
    end
    f.actions
  end

end
