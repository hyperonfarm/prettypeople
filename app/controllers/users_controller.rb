class UsersController < ApplicationController
  before_action :set_user, :only => [:show, :edit, :set_password]

  def show
  end

  def edit
  end

  def set_password
    respond_to do |format|
      if @user.update(password_params)
        # Sign in the user by passing validation in case their password changed
        sign_in @user, :bypass => true
        format.js {render :js => "window.location.href='"+root_path+"'"}
      else
        format.js { render 'error_password_set', layout: false }
      end
    end
  end

  def update
    respond_to do |format|
      if params[:user][:email] or params[:user][:name] or params[:user][:surname]
        if current_user.update(user_nopassword_params)
          format.js { render 'success', layout: false }
        else
          format.js { render 'error', layout: false }
        end

      elsif params[:user][:current_password] or params[:user][:password]
        if current_user.update_with_password(user_password_params)
          format.js { render 'success', layout: false }
        else
          format.js { render 'error', layout: false }
        end
      end
    end
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
        format.js { render 'success_create', layout: false }
      else
        format.html { redirect_to root_path, notice: 'User has not be created.' }
        format.js { render 'error_create', layout: false }
      end
    end
  end

  def favorites
    @favorites = Favorite.where(user: current_user)
  end

  def destroy_social_account
    @account = SocialAccount.find(params[:account_id])
    @account.destroy
    redirect_to :back
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def password_params
    params.require(:user).permit(:password, :password_confirmation, :name)
  end

  def user_params
    params.require(:user).permit(:name, :surname, :email, :current_password, :password, :password_confirmation, :avatar)
  end

  def user_password_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end

  def user_nopassword_params
    params.require(:user).permit(:name, :surname, :email)
  end

end

