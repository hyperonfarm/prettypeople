class PhonecallsController < ApplicationController
  def create
    @phonecall = Phonecall.new(phonecall_params)
    respond_to do |format|
      if @phonecall.save
        @phonecall.send_notifications
        format.js { render 'success', layout: false }
        format.html { redirect_to root_path }
      else
        format.js { render 'error', layout: false  }
      end
    end
  end

private

  def phonecall_params
    params.require(:phonecall).permit(:phone)
  end
end
