class MessagesController < ApplicationController
  def create
    @message = Message.new(message_params)
    respond_to do |format|
      if @message.save
        format.js { render 'success', layout: false }
        format.html { redirect_to root_path }
      else
        format.js { render 'error', layout: false  }
      end
    end
  end

private

  def message_params
    params.require(:message).permit(:name, :email, :text)
  end
end
