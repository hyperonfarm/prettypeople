class PostsController < ApplicationController
  def index
    @posts = Post.published.all

    @page = Page.find_by(:title => "posts")
    set_seo(@page)
  end

  def show
    @post = Post.friendly.find(params[:id])
    @other_posts = Post.where('id != ?', @post.id).limit(3)
    set_seo(@post)
  end

end
