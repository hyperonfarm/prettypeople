class CartController < ApplicationController

  def show
    @order = current_order

    respond_to do |format|
      format.html
    end
  end

  def add
    @line_item = LineItem.where(product_id: params[:product_id],
                                order: current_order).first_or_initialize
    @line_item.quantity += 1
    @line_item.save

    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def update
    @order = current_order
    @order.update(update_params)

    respond_to do |format|
      if params[:noresponse]
        format.js { render nothing: true }
      else
        format.js { render :layout => false }
      end
    end
  end

  def checkout
    @order = current_order
    @order.update(checkout_params)

    @order.send_notifications

    reset_current_order

    respond_to do |format|
      format.js { render 'checkout', layout: false }
    end
  end

  def disabled
    respond_to do |format|
      format.js
    end
  end

  private

  def update_params
    params.require(:order)
          .permit(:delivery, :city, :department, :phone, line_items_attributes: [:id, :quantity, :_destroy])
  end

  def checkout_params
    update_params.merge({ordered_at: Time.now, condition: "Новый"})
  end
end
