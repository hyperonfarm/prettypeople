class StaticController < ApplicationController
  def contacts
    @page = Page.find_by(:title => "contacts")
    set_seo(@page)
  end
  def feedbacks
    @page = Page.find_by(:title => "feedbacks")
    set_seo(@page)
  end
  def about
    @page = Page.find_by(:title => "about")
    set_seo(@page)
  end
end
