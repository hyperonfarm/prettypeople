class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :prepare_all

  helper_method :current_order

  def set_seo(page)
    set_meta_tags :site => 'PrettyPeople', :title => page.seo_title ? page.seo_title : '', :reverse => true,
                  :description => page.seo_description ? page.seo_description : "",
                  :keywords => page.seo_keywords ? page.seo_keywords : ""
  end

  def current_order
    @current_order ||= Order.not_completed
                            .where(user: current_user)
                            .first_or_create if current_user
  end

  def reset_current_order
    @current_order = nil
  end

  protected

  def prepare_all
    @home_categories = Category.published.all
    @home_feedbacks = Message.published.all
    @promo_post = Post.promo.sample(1).last
    @promo_product = Product.promo.sample(1).last
    @faq = Question.all
    @random_category = Category.published.sample(1).last
    @product_count = Product.published.count
  end
end
