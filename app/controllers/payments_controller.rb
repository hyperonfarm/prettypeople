class PaymentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  protect_from_forgery except: :notify

  def notify
    params.permit!
    @order = Order.find(params[:order_id])

    if @order.update_attribute(:status, params[:status])
      if params[:status] == "success"
        Mailer.admin_payment(@order).deliver
        unless @order.user.email.blank?
          Mailer.customer_payment(@order).deliver
        end
      end
    end

  end

  private

  def order_attrs
    params.require(:order).permit(:status)
  end

end
