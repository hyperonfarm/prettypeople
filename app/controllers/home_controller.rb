class HomeController < ApplicationController
  def index
    @promo_category = Category.all.sample(1)

    @bestsellers = Product.all.sample(6)
    @new_arrivals = Product.where(badge: "Новинка").all

    @slides = Slide.all
    @small_slides = SmallSlide.all
    @first_banner = Banner.find_by(id: 1)
    @second_banner = Banner.find_by(id: 2)
    @third_banner = Banner.find_by(id: 3)

    @page = Page.find_by(:title => "home")
    set_seo(@page)

    if params[:order]
      @order = Order.find(params[:order])
      @liqpay_request = Liqpay::Request.new(
        :amount => @order.total,
        :currency => 'UAH',
        :order_id => @order.id,
        :description => "PrettyPeople заказ №#{@order.id}",
        :result_url => "http://pretty-people.com.ua?payment_status=#{@order.id}",
        :server_url => "http://pretty-people.com.ua/notify"
      )
    end

    if params[:payment_status]
      @order = Order.find(params[:payment_status])
    end

    if params[:reset_password_token] or params[:set_password]
      reset_password_token = Devise.token_generator.digest(self, :reset_password_token, params[:reset_password_token])
      @user = User.find_by(reset_password_token: reset_password_token)
    end
  end
end
