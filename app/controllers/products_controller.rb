class ProductsController < ApplicationController
  def index
    @products = Product.order('created_at DESC').all

    @page = Page.find_by(:title => "products")
    set_seo(@page)
  end

  def show
    @product = Product.friendly.find(params[:id])
    set_seo(@product)
  end

end
