class CategoriesController < ApplicationController
  def index
    @categories = Category.published.all

    @page = Page.find_by(:title => "categories")
    set_seo(@page)
  end

  def show
    @category = Category.friendly.find(params[:id])
    set_seo(@category)
  end

end
