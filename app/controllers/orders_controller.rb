class OrdersController < ApplicationController
  def index
    @orders = Order.where(user_id: current_user.id).order('created_at DESC').all

    @line_items_count = LineItem.joins(:order).where(orders: { user_id: current_user.id }).merge(Order.total_completed).count
    @complete_orders = Order.total_completed.where(user_id: current_user.id).order('created_at DESC').all
  end
end
