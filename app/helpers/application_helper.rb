module ApplicationHelper
  def price_format(price)
    price ||= 0
    number_with_precision(price,
      :precision => 2,
      :strip_insignificant_zeros => true)
  end
end
