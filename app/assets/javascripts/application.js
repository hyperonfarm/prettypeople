//= require jquery
//= require jquery_ujs
//= require jquery.ui.all

//= requiere ./application/thm/modernizr.js
//= require ./application/thm/bootstrap.js
//= require ./application/thm/jquery.flexisel.js
//= require ./application/thm/wow.min.js
//= require ./application/thm/jquery.transit.js
//= require ./application/thm/jquery.jcountdown.js
//= require ./application/thm/jquery.appear.js
//= require ./application/thm/owl.carousel.js
//= require ./application/thm/jquery.ticker.js
//= require ./application/thm/responsiveslides.min.js
//= require ./application/thm/jquery.elevateZoom-3.0.8.min.js
//= require ./application/thm/jquery.themepunch.plugins.min.js
//= require ./application/thm/jquery.themepunch.revolution.min.js
//= require ./application/thm/jquery.scrollTo-1.4.2-min.js
//= require ./application/thm/main.js

//= require ./application/order_form.js
//= require ./application/instafeed.js
//= require ./application/instafeed-settings.js
//= require ./application/map.js
//= require ./application/modals.coffee
