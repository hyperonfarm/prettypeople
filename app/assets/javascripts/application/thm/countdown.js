$(document).ready(function () {
  $('.timer-time').each(function( index ) {
    var date =  new Date($(this).attr('data-date'))
    $(this).countdown({until: new Date($(this).attr('data-date')), format: 'DHM',
      labels: ['','','','','','',''], layout:"<div>{dnn} <span class='cd-time'>дн.</span></div><div>{hnn} <span class='cd-time'>ч.</span></div><div>{mnn} <span class='cd-time'>мин.</span></div><div>{snn} <span class='cd-time'>сек.</span></div>"});
  })
})
