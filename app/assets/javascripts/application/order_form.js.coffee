class LineItemFields
  constructor: (@element, @onChange) ->
    @price = @element.data('price')

    @attachHandlers()

  attachHandlers: ->
    @element.find('.count-increase').click =>
      @changeQuantity(1)

    @element.find('.count-decrease').click =>
      @changeQuantity(-1)

    @element.find('.line-item-quantity').change =>
      @updateTotal()

  quantity: ->
    parseInt @element.find('.line-item-quantity').val()

  total: ->
    @quantity() * @price

  updateTotal: ->
    @element.find('.line-item-total').text(@total().toFixed(1))
    @onChange()

  changeQuantity: (change) ->
    newQuantity = @quantity() + change
    newQuantity = 1 if newQuantity < 1
    @element.find('.line-item-quantity').val(newQuantity)
    @updateTotal()

class @OrderForm
  constructor: (@element) ->
    @bindLineItems()

  bindLineItems: ->
    @lineItems = []
    @element.find('.line_item').each (index, lineItem) =>
      fields = new LineItemFields $(lineItem), =>
        @updateTotal()
      @lineItems.push fields

  save: ->
    form = @element.find('order_form')
    url = form.attr 'action'
    method = form.attr 'method'
    data = form.serializeArray()
    data.push
      name: 'update'
      value: 'true'
    data.push
      name: 'noresponse'
      value: 'true'
    $.ajax url,
      type: method,
      data: $.param(data)

  destroy: ->
    @save()
    @unbind_events()

  total: ->
    @lineItems.reduce ((sum, lineItem) -> sum + lineItem.total()), 0

  updateTotal: ->
    @element.find('.total').text(@total().toFixed(1))
