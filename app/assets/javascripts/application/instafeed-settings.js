$(document).ready(function () {
  var feed = new Instafeed({
    get: 'tagged',
    tagName: 'prettypeopleukrainepp',
    clientId: '76ecd2e9fc9e460292de63d1fc43cb29',
    sortBy: 'most-recent',
    limit: 9,
    template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}" alt=""/ class="img-responsive"></a></li>',
    error: function() {}
  });
  feed.run();
})
