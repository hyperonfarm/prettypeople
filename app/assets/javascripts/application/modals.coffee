$ ->
  $('#registernow').on 'click', ->
    $('#login_alt').modal('hide')
    $('#registration').modal('show')
    false

  $('#reset_password_button').on 'click', ->
    $('#login_alt').modal('hide')
    $('#reset_password').modal('show')
    false

  $('#reset_password_button_alt').on 'click', ->
    $('#login').modal('hide')
    $('#reset_password').modal('show')
    false
