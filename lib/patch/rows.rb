module ActiveAdmin
  module Views
    class AttributesTable < ActiveAdmin::Component
      def row_t(*args, &block)
        row(args.first) do
          @collection.each do |element|
            element.translations.each do |translation|
              at = attributes_table_for(translation)
              at.row(:locale)
              at.row(args.first) do
                translation.send(args.first).html_safe
              end
              at
            end
          end
        end
      end

      def row_i(*args, &block)
        @collection.each do |element|
          url = element.send(args.first).try(:url, args.second)
          row(args.first) do
            link_to url do
              image_tag url, style: 'max-width: 30%'
            end
          end
        end
      end

      def row_h(*args, &block)
        @collection.each do |element|
          row(args.first) do
            element.send(args.first).try(:html_safe)
          end
        end
      end

      def row_l(*args, &block)
        @collection.each do |element|
          content = element.send(args.first)
          row(args.first) do
            I18n.l content unless content.blank?
          end
        end
      end
    end
  end
end
