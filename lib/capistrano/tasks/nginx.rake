namespace :deploy do
  namespace :nginx do
    task :symlink do
      on roles(:web) do
        within '/home/tsipa/conf/' do
          execute :ln, "-fs",
            "#{release_path}/config/nginx/#{fetch(:stage)}.conf",
            "#{fetch(:application)}_#{fetch(:stage)}.conf"
        end
      end
    end
  end
end
