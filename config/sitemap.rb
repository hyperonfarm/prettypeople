SitemapGenerator::Sitemap.default_host = Rails.application.config.action_controller.asset_host

SitemapGenerator::Sitemap.create(compress: (Rails.env != 'development')) do

  add contacts_path
  add feedbacks_path
  add categories_path
  add products_path
  add posts_path

  Category.find_each do |category|
    add category_path(category), lastmod: category.updated_at
  end

  Product.find_each do |product|
    add product_path(product), lastmod: product.updated_at
  end

  Post.find_each do |post|
    add post_path(post), lastmod: post.updated_at
  end
end
