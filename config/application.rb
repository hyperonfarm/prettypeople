require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Prettypeople
  class Application < Rails::Application
    config.exceptions_app = self.routes

    config.i18n.default_locale = :ru
    I18n.enforce_available_locales = false

    config.time_zone = 'Kyiv'
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    Rails.application.config.assets.precompile += %w( ckeditor/* )
  end
end
