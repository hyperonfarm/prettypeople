ActiveAdmin.setup do |config|
  config.site_title = "PrettyPeople"

  config.site_title_link = "/"

  config.authentication_method = :authenticate_admin_user!
  config.current_user_method = :current_admin_user
  config.skip_before_filter :authenticate_user!

  config.logout_link_path = :destroy_admin_user_session_path

  config.root_to = 'admin_users#index'

  config.allow_comments = false

  config.show_comments_in_menu = false

  config.batch_actions = false

  config.namespace :admin do |admin|
    admin.build_menu do |menu|
      menu.add id: 'home', label: proc { I18n.t('admin.home') }, priority: 10
      menu.add id: 'settings', label: proc { I18n.t('admin.settings') }, priority: 11
    end
  end


end
