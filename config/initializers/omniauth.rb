Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET'],
    {
      :scope => 'email,user_birthday,read_stream',
      :display => 'page',
      :secure_image_url => true,
      :image_size => 'square'
    }

  provider :vkontakte, ENV['VKONTAKTE_KEY'], ENV['VKONTAKTE_SECRET'],
    {
      :scope => 'email',
      :display => 'page',
      :lang => 'en',
      :image_size => 'original',
      :secure_image_url => true
    }
end
