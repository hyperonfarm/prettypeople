Devise.setup do |config|
  config.mailer_sender = 'noreply@pretty-people.com.ua'

  require 'devise/orm/active_record'

  config.case_insensitive_keys = [ :email ]

  config.strip_whitespace_keys = [ :email ]

  config.skip_session_storage = [:http_auth]

  config.stretches = Rails.env.test? ? 1 : 10

  config.reconfirmable = false

  config.password_length = 8..128

  config.reset_password_within = 6.hours

  config.sign_out_via = :delete

  config.secret_key = '8ba56f871a604e78c7a9eb9936610ab53fa5d930a74f4a20b099c3d47f64954ccc528cdb4b9b2bc8cfb41104584c7f0fa5d07e4a4459a90933d55b63919d5886'
end
