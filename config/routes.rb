Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks",
                                       :confirmations => "confirmations",
                                       :passwords => "passwords",
                                       :sessions => "sessions" }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :products, only: [:index, :show]
  resources :posts, only: [:index, :show]
  resources :categories, only: [:index, :show]
  resources :orders, only: [:index]
  resources :messages, only: [:create]
  resources :phonecalls, only: [:create]
  resources :favorites, only: [:create, :destroy]

  resources :users, except: [:destroy, :index] do
    member do
      get :favorites
      patch :set_password
      delete :destroy_social_account
    end
  end

  controller :cart do
    get   '/cart',                 action: :show,     as: :cart
    get   '/cart/disabled',        action: :disabled, as: :disabled_cart
    post  '/cart/add/:product_id', action: :add,      as: :add_in_cart

    patch '/cart', constraints: -> (r) { r.params[:update].present? },   action: :update
    patch '/cart', constraints: -> (r) { r.params[:checkout].present? }, action: :checkout
  end

  controller :static do
    get 'contacts', as: :contacts
    get 'feedbacks', as: :feedbacks
    get 'about', as: :about
  end

  get '404', :to => 'errors#page_not_found'
  get '500', :to => 'errors#server_error'

  controller :payments do
    post 'notify', as: :notify
  end

  post '/liqpay_payment' => 'payments#liqpay_payment'

  root 'home#index'

end
