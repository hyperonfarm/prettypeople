print 'SEO.......'

pages = Page.create([{
  title: 'home',
  seo_title: 'Главная',
  seo_keywords: '',
  seo_description: ''
}, {
  title: 'categories',
  seo_title: 'Категории',
  seo_keywords: '',
  seo_description: ''
}, {
  title: 'products',
  seo_title: 'Продукты',
  seo_keywords: '',
  seo_description: ''
}, {
  title: 'posts',
  seo_title: 'Новости',
  seo_keywords: '',
  seo_description: ''
}, {
  title: 'faq',
  seo_title: 'Faq',
  seo_keywords: '',
  seo_description: ''
}, {
  title: 'feedbacks',
  seo_title: 'Отзывы',
  seo_keywords: '',
  seo_description: ''
}, {
  title: 'contacts',
  seo_title: 'Контакты',
  seo_keywords: '',
  seo_description: ''
}])

puts "OK \n\n"
