print 'Slides.......'

slides = Slide.create([{
  title: 'Заголовок',
  subtitle: 'Подзаголовок',
  priority: 100,
  style: '1',
  image: open_file('slides/refectoci2.jpg')
}, {
  title: 'Заголовок',
  subtitle: 'Подзаголовок',
  priority: 90,
  style: '2',
  image: open_file('slides/refectoci.jpg')
}, {
  title: 'Заголовок',
  subtitle: 'Подзаголовок',
  priority: 80,
  style: '1',
  image: open_file('slides/refectoci3.jpg')
}])

puts "OK \n\n"
