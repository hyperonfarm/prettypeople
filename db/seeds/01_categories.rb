print 'Categories.......'

categories = Category.create([{
  title: 'RefectoCil',
  position: 100,
  published: true,
  image: open_file('categories/refecto.jpg')
}, {
  title: 'RefectoCil sensitive',
  description: 'RefectoCil Sensitive - первая краска для бровей и ресниц предназначена для чувствительных глаз и кожи, изготовлена на растительной основе!',
  position: 90,
  published: true,
  image: open_file('categories/refecto_sens.jpg')
}, {
  title: 'AWF Color',
  description: 'Все краски марки AWF COLOR офтальмологически тестированы и при правильном применении не вызывают аллергических реакций. Регулярное применение краски усиливает стойкость цвета, его глубину и интенсивность. Цвет: графит, 15 мл. Производитель: AWF COLOR (Германия)',
  position: 80,
  published: true,
  image: open_file('categories/awf.jpg')
}, {
  title: 'MYscara',
  description: 'Полуперманентная тушь MYscara - объемное окрашивание ресниц,  которое держится 10-20 дней.',
  position: 70,
  published: true,
  image: open_file('categories/myscara.jpg')
}, {
  title: 'Directions LaRiche',
  description: 'Временная краска для волос ярких  и натуральных оттенков. Палитра их 35 цветов,  что дает возможность смешивать цвета между собой. Краскадержится на волосах  2 - 4 недели. Позвольте себе быть ярче!',
  position: 60,
  published: true,
  image: open_file('categories/directions.jpg')
}, {
  title: 'Инструменты и аксессуары',
  position: 50,
  published: true,
  image: open_file('categories/instruments.jpg')
}, {
  title: 'Обучение',
  position: 40,
  published: true,
  image: open_file('categories/training.jpg')
}, {
  title: 'Услуги',
  description: 'Салон красоты',
  position: 30,
  published: true,
  image: open_file('categories/servis.jpg')
}])

puts "OK \n\n"
