print 'Questions.......'

questions = Question.create([{
  title: 'Почему PrettyPeople?',
  answer: 'Мы предлагаем только лучшее и специализируемся только на самом качественном продукте',
  priority: 100
}, {
  title: 'Оплата и доставка',
  answer: 'Если Вы не хотите ждать курьерскую доставку и имеете час свободного времени, можете приехать к нам по адресу ул. Анри Барбюса 22/26 и забрать свой заказ лично., Пожалуйста, обязательно предварительно договоритесь по телефону',
  priority: 80
}, {
  title: 'Вопрос 3?',
  answer: 'Здесь когда нибудь будет самый конструктивный и вычерпывающий ответ на вопрос номер один. Но пока этот раздел дорабатывается и причесывается',
  priority: 70
}, {
  title: 'Вопрос 4?',
  answer: 'Здесь когда нибудь будет самый конструктивный и вычерпывающий ответ на вопрос номер один. Но пока этот раздел дорабатывается и причесывается',
  priority: 60
}, {
  title: 'Вопрос 5?',
  answer: 'Здесь когда нибудь будет самый конструктивный и вычерпывающий ответ на вопрос номер один. Но пока этот раздел дорабатывается и причесывается',
  priority: 50
}, {
  title: 'Вопрос 6?',
  answer: 'Здесь когда нибудь будет самый конструктивный и вычерпывающий ответ на вопрос номер один. Но пока этот раздел дорабатывается и причесывается',
  priority: 40
}, {
  title: 'Вопрос 7?',
  answer: 'Здесь когда нибудь будет самый конструктивный и вычерпывающий ответ на вопрос номер один. Но пока этот раздел дорабатывается и причесывается',
  priority: 30
}])

puts "OK \n\n"
