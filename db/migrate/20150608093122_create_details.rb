class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.string :title
      t.string :value

      t.references :product

      t.timestamps
    end
  end
end
