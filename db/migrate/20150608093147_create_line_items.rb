class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :order, index: true
      t.references :product, index: true

      t.float :price
      t.integer :quantity, default: 0

      t.timestamps
    end
  end
end
