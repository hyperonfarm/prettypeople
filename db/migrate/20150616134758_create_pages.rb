class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title, :seo_title
      t.text :seo_keywords, :seo_description

      t.timestamps
    end
  end
end
