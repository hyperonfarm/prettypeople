class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.text :text

      t.attachment :image

      t.boolean :published, default: false
      t.boolean :promo, default: false

      t.string :seo_title
      t.text :seo_keywords, :seo_description

      t.timestamps
    end
  end
end
