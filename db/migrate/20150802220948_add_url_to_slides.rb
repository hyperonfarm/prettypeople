class AddUrlToSlides < ActiveRecord::Migration
  def change
    add_column :slides, :url, :text
  end
end
