class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :condition
      t.string :status
      t.string :delivery
      t.string :city
      t.string :department
      t.string :phone

      t.datetime :ordered_at

      t.text :admin_comment

      t.references :user, index: true

      t.timestamps
    end
  end
end
