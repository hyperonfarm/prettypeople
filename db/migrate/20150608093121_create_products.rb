class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :article
      t.text :description
      t.text :full_description

      t.decimal :price, :precision => 7, :scale => 3
      t.decimal :old_price, :precision => 7, :scale => 3

      t.boolean :published, :default => false

      t.string :badge
      t.string :discount_badge

      t.attachment :image

      t.boolean :promo_on, :default => false
      t.datetime :promo_date

      t.references :category

      t.string :slug

      t.string :seo_title
      t.text :seo_keywords, :seo_description

      t.timestamps
    end
  end
end
