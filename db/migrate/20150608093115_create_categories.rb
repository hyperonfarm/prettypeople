class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.text :description

      t.integer :position
      t.boolean :published, :default => false

      t.attachment :image

      t.string :slug
      t.string :seo_title
      t.text :seo_keywords, :seo_description

      t.timestamps
    end
  end
end
