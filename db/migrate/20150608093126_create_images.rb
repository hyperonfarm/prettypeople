class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.attachment :file

      t.references :product

      t.timestamps
    end
  end
end
