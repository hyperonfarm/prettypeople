class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :name
      t.string :email
      t.text :text

      t.boolean :published, default: false

      t.timestamps
    end
  end
end
