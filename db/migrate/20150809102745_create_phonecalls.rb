class CreatePhonecalls < ActiveRecord::Migration
  def change
    create_table :phonecalls do |t|
      t.string :phone

      t.timestamps
    end
  end
end
