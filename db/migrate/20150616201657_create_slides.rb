class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.attachment :image
      t.string :title, :subtitle, :style

      t.integer :priority

      t.timestamps
    end
  end
end
