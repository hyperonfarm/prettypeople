class CreateSmallSlides < ActiveRecord::Migration
  def change
    create_table :small_slides do |t|
      t.string :title
      t.string :url

      t.attachment :image

      t.timestamps
    end
  end
end
