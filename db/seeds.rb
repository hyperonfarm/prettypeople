def open_file(path)
  File.open(Rails.root.join('test/assets', path))
end

puts '*** Seeding ****'

Dir[Rails.root.join('db/seeds/*.rb')].sort.each { |f| require f }

puts "*** OK *********\n\n"
